Feature: StoreLocator
@regression 
Scenario: Verify Store Locator with Valid Postcode

Given I am on Store Locator Page
When I enter valid data
Then I should see nearest stores in list and map view
@regression
Scenario: Verify Store Locator with Invalid Postcode

Given I am on Store Locator Page
When I enter invalid data
Then I should see no stores 
And I should see error message

@smoke
Scenario: Verify Store Locator with Blank field

Given I am on Store Locator Page
When I leave the field blank
Then I should see no products 
And I should see an error message

Scenario: Verify Store Locator with Valid Data with Click and Collect

Given I am on Store Locator Page 
When I enter valid data 
And I select Click and Collect 
Then I should see related stores in list and map views

Scenario: Verify Store Locator with Invalid Data with Click and Collect

Given I am on Store Locator Page
When I enter invalid data
And I select Click and Collect 
Then I should see no results

Scenario: Verify Store Locator with valid data and category 

Given I am on Store Locator Page
When I enter valid data
And I select valid category
Then I should see related store in list and map views

Scenario: Verify Store Locator with Invalid data and valid category 

Given I am on Store Locator Page
When I enter invalid data
And I select valid category
Then I should see not results
And I should see the error message
