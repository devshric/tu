Feature: Search 
 @regression @smoke
Scenario: Verify Search with Valid Data
Given I am on Home Page
When I search for the valid product	
Then I should see the related results

@regression
Scenario: Verify Search with Invalid Data

Given I am on Home Page
When I search for the invalid product
Then I should see no products found
And error message should be displayed.

Scenario: Verify Search with blank field

Given I am on Home Page
When I leave the search field blank 
Then I should be prompted to complete the product search

Scenario: Verify Search with Special Characters

Given I am on Home Page
When I enter special characters in Search field
Then I should see randomly selected products

Scenario: Verify Search with auto suggestions

Given I am on Home Page
When I enter something in Search field
Then Auto suggestion should be displayed

