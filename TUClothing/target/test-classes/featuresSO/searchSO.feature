Feature: SearchScenarioOutline
 
Scenario Outline: Verify Search with Valid Data
Given I am on Home Page
When I search with "<Search Term>" search term
Then I should see the related results

Examples:
|Search Term|
|jeans|
|136397490|
|Men|

Scenario Outline: Verify Search with Invalid Data
Given I am on Home Page
When I search with "<Search Term>" search term
Then I should see the related results

Examples:
|Search Term|
|hjgjhgjh|
|12345678|
|cash|

