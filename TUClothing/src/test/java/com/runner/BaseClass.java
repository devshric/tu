package com.runner;

import org.openqa.selenium.WebDriver;

import com.pages.HomePage;
import com.pages.LoginPage;
import com.pages.MyAccountPage;
import com.pages.SearchResultsPage;
import com.pages.StoreLocatorPage;
import com.pages.StoreLocatorSearchResults;

public class BaseClass {
	public static WebDriver driver;
	
	public static HomePage homePage = new HomePage();
	public static SearchResultsPage searchResultsPage = new SearchResultsPage();
	public static StoreLocatorPage storeLocator = new StoreLocatorPage();
	public static StoreLocatorSearchResults storeLocatorSearchResults = new StoreLocatorSearchResults();
	public static LoginPage loginPage = new LoginPage();
	public static MyAccountPage myAccountPage = new MyAccountPage();
}
