package com.stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class SearchResultsPageSD extends BaseClass{
	@Then("^I should see the related results$")
	public void i_should_see_the_related_results() throws Throwable {
		searchResultsPage.relatedProducts();
		
	}
		
		@Then("^error message should be displayed\\.$")
		public void error_message_should_be_displayed() throws Throwable {
			searchResultsPage.noProductsFound();
			
		}
			@Then("^I should see randomly selected products$")
			public void i_should_see_randomly_selected_products() throws Throwable {
				searchResultsPage.randomResults();
				 
				
			    
		
	 

}
}
