package com.stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class MyAccountPageSD extends BaseClass {
	
	@Then("^I should successfully log in\\.$")
	public void i_should_successfully_log_in() throws Throwable {
		myAccountPage.successfulLogin();
	}
		
	@Then("^I should not log in$")
		public void i_should_not_log_in() throws Throwable {
		    
		}

	@Then("^I should see the message ?Please check the fields below and try again?$")
		public void i_should_see_the_message_Please_check_the_fields_below_and_try_again() throws Throwable {
			myAccountPage.loginUnsuccessful();
}
}
