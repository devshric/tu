package com.stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllTestCases {
	public static By SEARCHTEXTBOX = By.cssSelector("#search");
	public static By SEARCHBUTTON = By.cssSelector(".searchButton");
	public static WebDriver driver;
	public static By STORELOCATOR = By.cssSelector("a[href='/store-finder']");
	public static By POSTCODEORTOWN = By.cssSelector("input[placeholder='Postcode or town']");
	public static By FINDSTOREBUTTON = By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
	public static By REGISTERBUTTON = By.cssSelector("a[href='/login']");
	public static By EMAILADDRESS = By.cssSelector("#register_email");
	public static By FIRSTNAME = By.cssSelector("#register_firstName");
	public static By LASTNAME = By.cssSelector("#register_lastName");
	public static By NEWPASSWORD = By.cssSelector("#password");
	public static By CONFIRMPWD = By.cssSelector("#register_checkPwd");
	public static By SUBMITBUTTON = By.cssSelector("#submit-register");
	public static By LOGIN = By.cssSelector("a[href='/login']");
	public static By USERNAME = By.cssSelector("#j_username");
	public static By PASSWORD = By.cssSelector("#j_password");
	public static By LOGINBUTTON = By.cssSelector("#submit-login");
	public static By CHECKOUTBUTTON = By.cssSelector("a[data-testid='checkoutButton']");
	public static By CHECKOUTBUTTONBOTTOM = By.cssSelector("#basketButtonBottom");
	
	
	@Given("^I am on Home Page$")
	public void i_am_on_Home_Page() throws Throwable {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.get("https://tuclothing.sainsburys.co.uk/");
		Assert.assertEquals("Womens, Mens, Kids & Baby Fashion  | Tu clothing", driver.getTitle());

	}

	@When("^I search for the valid product$")
	public void i_search_for_the_valid_product() throws Throwable {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jeans");
		driver.findElement(SEARCHBUTTON).click();
		

	   
	}

	@Then("^I should see the related results$")
	public void i_should_see_the_related_results() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=jeans", driver.getCurrentUrl());
	   
	}

	@When("^I search for the invalid product$")
	public void i_search_for_the_invalid_product() throws Throwable {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jhkjhkjh");
		driver.findElement(SEARCHBUTTON).click();
	
	}
	

	@Then("^I should see no products found$")
	public void i_should_see_no_products_found() throws Throwable {
		
		
	    
	}

	@Then("^error message should be displayed\\.$")
	public void error_message_should_be_displayed() throws Throwable {
	Assert.assertEquals("Sorry, no results for 'jhkjhkjh", driver.findElement(By.cssSelector("h1")).getText());
 
	}

	@When("^I leave the search field blank$")
	public void i_leave_the_search_field_blank() throws Throwable {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("");
		driver.findElement(SEARCHBUTTON).click();
		
	    
	    
	}

	@Then("^I should be prompted to complete the product search$")
	public void i_should_be_prompted_to_complete_the_product_search() throws Throwable {
	    
	}

	@When("^I enter special characters in Search field$")
	public void i_enter_special_characters_in_Search_field() throws Throwable {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("$$$$$$");
		driver.findElement(SEARCHBUTTON).click();
	   
	}

	@Then("^I should see randomly selected products$")
	public void i_should_see_randomly_selected_products() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=%24%24%24%24%24%24", driver.getCurrentUrl());
		 
		
	    
	}

	@When("^I enter something in Search field$")
	public void i_enter_something_in_Search_field() throws Throwable {
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys("jea");
		driver.findElement(SEARCHBUTTON).click();
	   
	}

	@Then("^Auto suggestion should be displayed$")
	public void auto_suggestion_should_be_displayed() throws Throwable {
	   
	}
	

	@Given("^I am on Store Locator Page$")
	public void i_am_on_Store_Locator_Page() throws Throwable {
		driver.findElement(STORELOCATOR).click();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
	    
	}

	@When("^I enter valid data$")
	public void i_enter_valid_data() throws Throwable {
		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
	   
	}

	@Then("^I should see nearest stores in list and map view$")
	public void i_should_see_nearest_stores_in_list_and_map_view() throws Throwable {
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW9+9BP&men=on&CSRFToken=6d30bd50-01e4-4849-9e60-dc416fcd0157", driver.getCurrentUrl());
	   
	}

	@When("^I enter invalid data$")
	public void i_enter_invalid_data() throws Throwable {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();

	    
	}

	@Then("^I should see no stores$")
	public void i_should_see_no_stores() throws Throwable {
		
	    
	}

	@Then("^I should see error message$")
	public void i_should_see_error_message() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW+9BP&CSRFToken=8ef16c1d-fbc6-4b9c-841d-02665ac1531d", driver.getCurrentUrl());
		
	    
	}

	@When("^I leave the field blank$")
	public void i_leave_the_field_blank() throws Throwable {
		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
	   
	}

	@Then("^I should see no products$")
	public void i_should_see_no_products() throws Throwable {
	    
	}

	@Then("^I should see an error message$")
	public void i_should_see_an_error_message() throws Throwable {
	   
	}

	@When("^I select Click and Collect$")
	public void i_select_Click_and_Collect() throws Throwable {
		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();
		driver.findElement(By.cssSelector("label[for='click']")).click();
		driver.findElement(FINDSTOREBUTTON).click();
	}

	@Then("^I should see related stores in list and map views$")
	public void i_should_see_related_stores_in_list_and_map_views() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW9+9BP&men=on&click=on&CSRFToken=73cff779-7e21-4cb4-bea9-8448f5566e65",driver.getCurrentUrl());
	}
	
		@Given("^I am on Login Page$")
		public void i_am_on_Login_Page() throws Throwable {
			driver.findElement(LOGIN).click();
			Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		   
		}

		@When("^I enter valid details$")
		public void i_enter_valid_details() throws Throwable {
			driver.findElement(USERNAME).clear();
			driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
			driver.findElement(PASSWORD).clear();
			driver.findElement(PASSWORD).sendKeys("abc123");
			driver.findElement(LOGINBUTTON).click();
		   
		}

		@Then("^I should successfully log in\\.$")
		public void i_should_successfully_log_in() throws Throwable {
			Assert.assertEquals("Womens, Mens, Kids & Baby Fashion  | Tu clothing", driver.getTitle());
		
		}
		  

		@When("^I enter invalid details$")
		public void i_enter_invalid_details() throws Throwable {
			driver.findElement(USERNAME).clear();
			driver.findElement(USERNAME).sendKeys("disha@0912@yahoo.com");
			driver.findElement(PASSWORD).clear();
			driver.findElement(PASSWORD).sendKeys("abc1234");
			driver.findElement(LOGINBUTTON).click();
		    
		}

		@Then("^I should not log in$")
		public void i_should_not_log_in() throws Throwable {
		    
		}

		@Then("^I should see the message ?Please check the fields below and try again?$")
		public void i_should_see_the_message_Please_check_the_fields_below_and_try_again() throws Throwable {
			Assert.assertEquals("Please check the fields below and try again", driver.findElement(By.cssSelector("p")).getText());
		   
		}

		@When("^I leave the fields blank$")
		public void i_leave_the_fields_blank() throws Throwable {
			driver.findElement(USERNAME).clear();
			driver.findElement(USERNAME).sendKeys("");
			driver.findElement(PASSWORD).clear();
			driver.findElement(PASSWORD).sendKeys("");
			driver.findElement(LOGINBUTTON).click();
		   
		}

		@Then("^Log In button should not be enabled\\.$")
		public void log_In_button_should_not_be_enabled() throws Throwable {
		   
		}

		@When("^I enter Valid details$")
		public void i_enter_Valid_details() throws Throwable {
			driver.findElement(USERNAME).clear();
			driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
			driver.findElement(PASSWORD).clear();
			driver.findElement(PASSWORD).sendKeys("abc123");
			driver.findElement(LOGINBUTTON).click();
			
		   
		}

		@When("^I do not tick Captcha box$")
		public void i_do_not_tick_Captcha_box() throws Throwable {
		
		}

		@Then("^I should see the message ?Please check the captcha?$")
		public void i_should_see_the_message_Please_check_the_captcha() throws Throwable {
			Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("p")).getText());
		    
	
		    
		}

		@When("^I click on forgot password$")
		public void i_click_on_forgot_password() throws Throwable {
			driver.findElement(By.cssSelector("#forgot-password-link")).click();
		   
		}

		@Then("^I forgot password page should appear$")
		public void i_forgot_password_page_should_appear() throws Throwable {
			Assert.assertEquals("Forgotten Tu password", driver.findElement(By.cssSelector("h2")).getText());
		   
		}

		@Then("^I should be asked to enter the registered email$")
		public void i_should_be_asked_to_enter_the_registered_email() throws Throwable {
		    
		}
		@Given("^I am on the Registration Page$")
		public void i_am_on_the_Registration_Page() throws Throwable {
			driver.findElement(REGISTERBUTTON).click();
			driver.findElement(By.cssSelector(".regToggle")).click();
			Assert.assertEquals("Login | Tu clothing", driver.getTitle());
		    
		}

		@When("^I enter all valid details$")
		public void i_enter_all_valid_details() throws Throwable {
			driver.findElement(EMAILADDRESS).clear();
			driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
			Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
			titleDropDown.selectByValue("mrs");
			driver.findElement(FIRSTNAME).clear();
			driver.findElement(FIRSTNAME).sendKeys("Disha");
			driver.findElement(LASTNAME).clear();
			driver.findElement(LASTNAME).sendKeys("Chandel");
			driver.findElement(NEWPASSWORD).clear();
			driver.findElement(NEWPASSWORD).sendKeys("test123");
			driver.findElement(CONFIRMPWD).clear();
			driver.findElement(CONFIRMPWD).sendKeys("test123");

			driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
			driver.findElement(SUBMITBUTTON).click();

		  
		}

		@Then("^I should be able to successfully register\\.$")
		public void i_should_be_able_to_successfully_register() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/register/success", driver.getCurrentUrl());
		}

		@When("^I enter the invalid email address$")
		public void i_enter_the_invalid_email_address() throws Throwable {
			driver.findElement(EMAILADDRESS).clear();
			driver.findElement(EMAILADDRESS).sendKeys("abc@0912@yahoo.com");
			Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
			titleDropDown.selectByValue("mrs");
			driver.findElement(FIRSTNAME).clear();
			driver.findElement(FIRSTNAME).sendKeys("Disha");
			driver.findElement(LASTNAME).clear();
			driver.findElement(LASTNAME).sendKeys("Chandel");
			driver.findElement(NEWPASSWORD).clear();
			driver.findElement(NEWPASSWORD).sendKeys("test123");
			driver.findElement(CONFIRMPWD).clear();
			driver.findElement(CONFIRMPWD).sendKeys("test123");

			driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
			driver.findElement(SUBMITBUTTON).click();

			
		   
		}

		@Then("^I should not be able to register$")
		public void i_should_not_be_able_to_register() throws Throwable {
			Assert.assertEquals("Please correct the errors below.", driver.findElement(By.cssSelector("p")).getText());
		   
		}

		@When("^I leave any mandatory field blank$")
		public void i_leave_any_mandatory_field_blank() throws Throwable {
			driver.findElement(EMAILADDRESS).clear();
			driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
			Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
			titleDropDown.selectByValue("mrs");
			driver.findElement(FIRSTNAME).clear();
			driver.findElement(FIRSTNAME).sendKeys("");
			driver.findElement(LASTNAME).clear();
			driver.findElement(LASTNAME).sendKeys("");
			driver.findElement(NEWPASSWORD).clear();
			driver.findElement(NEWPASSWORD).sendKeys("test123");
			driver.findElement(CONFIRMPWD).clear();
			driver.findElement(CONFIRMPWD).sendKeys("test123");

			driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
			driver.findElement(SUBMITBUTTON).click();

		    
		}

		@Then("^I should see an error message$")
		public void i_should_see_an_error_message1() throws Throwable {
			
		    
		}

		@When("^I enter the existing email$")
		public void i_enter_the_existing_email() throws Throwable {
			driver.findElement(EMAILADDRESS).clear();
			driver.findElement(EMAILADDRESS).sendKeys("disha_0912@yahoo.com");
			Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
			titleDropDown.selectByValue("mrs");
			driver.findElement(FIRSTNAME).clear();
			driver.findElement(FIRSTNAME).sendKeys("Disha");
			driver.findElement(LASTNAME).clear();
			driver.findElement(LASTNAME).sendKeys("Chandel");
			driver.findElement(NEWPASSWORD).clear();
			driver.findElement(NEWPASSWORD).sendKeys("test123");
			driver.findElement(CONFIRMPWD).clear();
			driver.findElement(CONFIRMPWD).sendKeys("test123");

			driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
			driver.findElement(SUBMITBUTTON).click();

		}

		@Then("^I should see error message$")
		public void i_should_see_error_message1() throws Throwable {
			Assert.assertEquals("You already have a Tu account. Please login to continue.", driver.findElement(By.cssSelector("span")).getText());
		    
		}

		@When("^I enter different values in password and confirm password$")
		public void i_enter_different_values_in_password_and_confirm_password() throws Throwable {
			driver.findElement(EMAILADDRESS).clear();
			driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
			Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
			titleDropDown.selectByValue("mrs");
			driver.findElement(FIRSTNAME).clear();
			driver.findElement(FIRSTNAME).sendKeys("Disha");
			driver.findElement(LASTNAME).clear();
			driver.findElement(LASTNAME).sendKeys("Singh");
			driver.findElement(NEWPASSWORD).clear();
			driver.findElement(NEWPASSWORD).sendKeys("test123");
			driver.findElement(CONFIRMPWD).clear();
			driver.findElement(CONFIRMPWD).sendKeys("test1234");

			driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
			driver.findElement(SUBMITBUTTON).click();

		}

		@Then("^I should see an error$")
		public void i_should_see_an_error() throws Throwable {
			Assert.assertEquals("Password and password confirmation do not match", driver.findElement(By.cssSelector("span")).getText());
		   
		}

		@When("^I enter invalid nectar card$")
		public void i_enter_invalid_nectar_card() throws Throwable {
			driver.findElement(EMAILADDRESS).clear();
			driver.findElement(EMAILADDRESS).sendKeys("abc_0912@yahoo.com");
			Select titleDropDown = new Select(driver.findElement(By.cssSelector("#register_title")));
			titleDropDown.selectByValue("mrs");
			driver.findElement(FIRSTNAME).clear();
			driver.findElement(FIRSTNAME).sendKeys("Disha");
			driver.findElement(LASTNAME).clear();
			driver.findElement(LASTNAME).sendKeys("Singh");
			driver.findElement(NEWPASSWORD).clear();
			driver.findElement(NEWPASSWORD).sendKeys("test123");
			driver.findElement(CONFIRMPWD).clear();
			driver.findElement(CONFIRMPWD).sendKeys("test123");

			driver.findElement(By.xpath("//label[@class='ln-c-form-option__label']")).click();
			driver.findElement(By.cssSelector("#regNectarPointsOne")).clear();
			driver.findElement(By.cssSelector("#regNectarPointsOne")).sendKeys("1234556");
			driver.findElement(By.cssSelector("#regNectarPointsTwo")).clear();
			driver.findElement(By.cssSelector("#regNectarPointsTwo")).sendKeys("55A");
			driver.findElement(SUBMITBUTTON).click();

		    
		}

		@Then("^I should see the error message$")
		public void i_should_see_the_error_message() throws Throwable {
			Assert.assertEquals("Please correct the errors below.", driver.findElement(By.cssSelector("p")).getText());
		    
		}

	
}





