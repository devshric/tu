package com.stepDefinition;

import org.junit.Assert;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class LoginPageSD extends BaseClass{

	@Given("^I am on Login Page$")
	public void i_am_on_Login_Page() throws Throwable {
		loginPage.verifyLoginPage();
				
}
	@When("^I enter valid details$")
	public void i_enter_valid_details() throws Throwable {
		loginPage.loginWithValidDetails();
	}
	
	@When("^I enter invalid details$")
	public void i_enter_invalid_details() throws Throwable {
		loginPage.loginWithInvalidDetails();
		
}
	@When("^I enter Valid details$")
	public void i_enter_Valid_details() throws Throwable {
		
		
	   
	}

	@When("^I do not tick Captcha box$")
	public void i_do_not_tick_Captcha_box() throws Throwable {
	
	}
}
