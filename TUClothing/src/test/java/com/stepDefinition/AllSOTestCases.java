package com.stepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllSOTestCases {
	@Given("^I am on Home Page$")
	public void i_am_on_Home_Page() throws Throwable {
	    
	}

	@When("^I search with \"([^\"]*)\" search term$")
	public void i_search_with_search_term(String searchWord) throws Throwable {
	   
	}

	@Then("^I should see the related results$")
	public void i_should_see_the_related_results() throws Throwable {
	   
	}

}
