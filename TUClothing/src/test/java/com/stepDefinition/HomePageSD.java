package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class HomePageSD extends BaseClass{
	
	
	@Given("^I am on Home Page$")
	public void i_am_on_Home_Page() throws Throwable {
		homePage.verifyHomePage();
			

}
	
	@When("^I search with \"([^\"]*)\" search term$")
	public void i_search_with_search_term(String searchWord) throws Throwable {
		homePage.searchForTheProducts(searchWord);
		
		
	}
	@When("^I leave the search field blank$")
	public void i_leave_the_search_field_blank() throws Throwable {
		homePage.blankSearchField();
	}
	
	@When("^I enter special characters in Search field$")
	public void i_enter_special_characters_in_Search_field() throws Throwable {
		homePage.searchWithSpecialCharacters();
	}
	}
		