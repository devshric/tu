package com.stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class StoreLocatorSD extends BaseClass {
	

	
	@Given("^I am on Store Locator Page$")
	public void i_am_on_Store_Locator_Page() throws Throwable {
		
		storeLocator.storeLocatorPage();
		
	
		
	}
	@When("^I enter valid data$")
	public void i_enter_valid_data() throws Throwable {
		storeLocator.searchWithValidPostcode();
	}
	
	@When("^I enter invalid data$")
	public void i_enter_invalid_data() throws Throwable {
		storeLocator.searchWithInvalidPostcode();
		
		
}
}
