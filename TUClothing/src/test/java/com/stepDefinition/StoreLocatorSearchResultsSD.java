package com.stepDefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class StoreLocatorSearchResultsSD extends BaseClass{
	@Then("^I should see nearest stores in list and map view$")
	public void i_should_see_nearest_stores_in_list_and_map_view() throws Throwable {
		
		storeLocatorSearchResults.nearestStores();
		

}
	@Then("^I should see no stores$")
	public void i_should_see_no_stores() throws Throwable {
		
	    
	}

	@Then("^I should see error message$")
	public void i_should_see_error_message() throws Throwable {
		storeLocatorSearchResults.errorMessage();	
	}
}

