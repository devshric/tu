package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class MyAccountPage extends BaseClass{
	
	public void successfulLogin() {
		
		Assert.assertEquals("Womens, Mens, Kids & Baby Fashion  | Tu clothing", driver.getTitle());
	}
public void loginUnsuccessful() {
	
	Assert.assertEquals("Please check the fields below and try again", driver.findElement(By.cssSelector("p")).getText());
	

}
}
