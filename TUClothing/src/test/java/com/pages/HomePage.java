package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

public class HomePage extends BaseClass {

	private static By SEARCHTEXTBOX = By.cssSelector("#search");
	private static By SEARCHBUTTON = By.cssSelector(".searchButton");

	public void verifyHomePage() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\CEX\\Documents\\DevshriClasses\\Automation\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.get("https://tuclothing.sainsburys.co.uk/");
		Assert.assertEquals("Womens, Mens, Kids & Baby Fashion  | Tu clothing", driver.getTitle());

	}

	public void searchForTheProducts(String searchWord) {

		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys(searchWord);
		driver.findElement(SEARCHBUTTON).click();
	}
	
	public void blankSearchField() {
		
	
	driver.findElement(SEARCHTEXTBOX).clear();
	driver.findElement(SEARCHTEXTBOX).sendKeys("");
	driver.findElement(SEARCHBUTTON).click();
}
	
	public void searchWithSpecialCharacters() {
		
	driver.findElement(SEARCHTEXTBOX).clear();
	driver.findElement(SEARCHTEXTBOX).sendKeys("$$$$$$");
	driver.findElement(SEARCHBUTTON).click();
}

}