package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class StoreLocatorPage extends BaseClass {
	public static By STORELOCATOR = By.cssSelector("a[href='/store-finder']");
	public static By POSTCODEORTOWN = By.cssSelector("input[placeholder='Postcode or town']");
	public static By FINDSTOREBUTTON = By.cssSelector("button[class='ln-c-button ln-c-button--primary']");

	public void storeLocatorPage() {
		
	driver.findElement(STORELOCATOR).click();
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder", driver.getCurrentUrl());
	}
	
	public void searchWithValidPostcode() {
		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW9 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();

		driver.findElement(FINDSTOREBUTTON).click();
	}
	public void searchWithInvalidPostcode() {
		driver.findElement(STORELOCATOR).click();

		driver.findElement(POSTCODEORTOWN).clear();
		driver.findElement(POSTCODEORTOWN).sendKeys("NW 9BP");
		driver.findElement(By.cssSelector("label[for='children']")).click();

	}

}

