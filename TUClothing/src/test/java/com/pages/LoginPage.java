package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class LoginPage extends BaseClass {
	public static By LOGIN = By.cssSelector("a[href='/login']");
	public static By USERNAME = By.cssSelector("#j_username");
	public static By PASSWORD = By.cssSelector("#j_password");
	public static By LOGINBUTTON = By.cssSelector("#submit-login");

	
	public void verifyLoginPage() {
	driver.findElement(LOGIN).click();
	Assert.assertEquals("Login | Tu clothing", driver.getTitle());
	}
	
	public void loginWithValidDetails() {
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha_0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc123");
		driver.findElement(LOGINBUTTON).click();
	}
	
	public void loginWithInvalidDetails() {
		driver.findElement(USERNAME).clear();
		driver.findElement(USERNAME).sendKeys("disha@0912@yahoo.com");
		driver.findElement(PASSWORD).clear();
		driver.findElement(PASSWORD).sendKeys("abc1234");
		driver.findElement(LOGINBUTTON).click();
	}

	
}


