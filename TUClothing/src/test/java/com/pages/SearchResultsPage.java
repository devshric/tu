package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class SearchResultsPage extends BaseClass  {
	
	public void relatedProducts() {
		
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=jeans", driver.getCurrentUrl());
	}
	
	public void noProductsFound() {
		
		Assert.assertEquals("Sorry, no results for 'jhkjhkjh", driver.findElement(By.cssSelector("h1")).getText());
	}
	
	public void randomResults() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=%24%24%24%24%24%24", driver.getCurrentUrl());
	}

}
