package com.pages;

import org.junit.Assert;

import com.runner.BaseClass;

public class StoreLocatorSearchResults extends BaseClass{
	
	public void nearestStores() {
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW9+9BP&men=on&CSRFToken=6d30bd50-01e4-4849-9e60-dc416fcd0157", driver.getCurrentUrl());

}
	public void errorMessage() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store-finder?q=NW+9BP&CSRFToken=8ef16c1d-fbc6-4b9c-841d-02665ac1531d", driver.getCurrentUrl());
	}
}
