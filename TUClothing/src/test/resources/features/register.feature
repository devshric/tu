Feature: Register 
@regression @smoke
Scenario: Verify Register with valid details

Given I am on the Registration Page
When I enter all valid details
Then I should be able to successfully register.
@regression
Scenario: Verify Register with invalid email

Given I am on the Registration Page
When I enter the invalid email address
Then I should not be able to register

Scenario: Verify Register with all manadatory fields not filled

Given I am on the Registration Page
When I leave any mandatory field blank
Then I should see an error message

Scenario: Verify Register with existing email

Given I am on the Registration Page
When I enter the existing email
Then I should see error message 

Scenario: Verify Register with different password and confirm password

Given I am on the Registration Page
When I enter different values in password and confirm password
Then I should see an error

Scenario: Verify Register with different password and confirm password

Given I am on the Registration Page
When I enter invalid nectar card
Then I should see the error message
