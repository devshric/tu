Feature: Login 
@regression 
Scenario: Verify Login with Valid Details

Given I am on Login Page
When I enter valid details
Then I should successfully log in.


@regression
Scenario: Verify Login with Invalid Details

Given I am on Login Page
When I enter invalid details
Then I should not log in 
And I should see the message �Please check the fields below and try again�

@regression
Scenario: Verify Login with Blank field.

Given I am on Login Page
When I leave the fields blank
Then Log In button should not be enabled. 

Scenario: Verify Login with Valid details without Captcha 

Given I am on Login Page
When I enter Valid details
And I do not tick Captcha box
Then I should not log in 
And I should see the message �Please check the captcha�

Scenario: Verify Forgot Password Feature

Given I am on Login Page
When I click on forgot password 
Then I forgot password page should appear
And I should be asked to enter the registered email

Scenario: Verify password being displayed in encrypted form

Given I am on Login Page
When I enter the password
Then Password should be displayed in encrypted form


Scenario: Verify Login with New Password

Given I am on Login Page
 And I have a new password
When login with old password
Then I should not log in
And I should see the error message �Please check the fields below and try again�
